<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class LogementController extends Controller {
    public function index() {
        return Inertia::render( 'Logements/Index' );
    }

    public function add() {
        return Inertia::render( 'Logements/AddLogementForm' );
    }
}

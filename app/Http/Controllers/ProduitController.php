<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class ProduitController extends Controller {
    public function index() {
        return Inertia::render( 'Produits/Index' );
    }

    public function add() {
        return Inertia::render( 'Produits/AddProduitForm' );
    }
}

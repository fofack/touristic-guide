<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PictureController extends Controller
{
    public function index()
    {
        $data = Picture::all();
        return Inertia::render('categories', 
        [
            'data' => $data->latest()->get()
        ]);
    }

    /**
     * Show the form for creaing a new resource.
     * 
     * @return Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'chemin' => ['required']
        ])->validate();

        Picture::create($request->all());
        return redirect()->back()->with('message', 'picture created successfully.');
    }

    /**
     * Show the form for creaing a new resource.
     * 
     * @return Response
     */
    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'chemin' => ['required']
        ])->validate();

        if($request->has('id'))
        {
            Picture::find($request->input('id'))->update($request->all());
            return redirect()->back()->with('message', 'picture Updated successfully.');
        }
    }

    /**
     * Show the form for creaing a new resource.
     * 
     * @return Response
     */
    public function delete(Request $request)
    {
     
        $request->has('id') ? 
        Picture::find($request->input('id'))->delete() :
                redirect()->back()
                    ->with('errors', 'Somethings goes wrong.');
        
        return redirect()->back()
                    ->with('message', 'picture deleted successfully.');
    }

}

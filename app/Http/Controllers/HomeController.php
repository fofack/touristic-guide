<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Models\SiteTouristique;

class HomeController extends Controller {
    public function index() {
        $sites = SiteTouristique::join( 'pictures', 'site_touristiques.image_id', 'pictures.id' )
        ->get()
        ->take( 6 );

        return Inertia::render( 'Home', [
            'sites' => $sites
        ] );
    }
}

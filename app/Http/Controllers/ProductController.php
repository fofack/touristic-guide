<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $data = Product::all();
        return Inertia::render('Products', 
        [
            'data' => $data->latest()->get()
        ]);
    }

    /**
     * Show the form for creaing a new resource.
     * 
     * @return Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'nom_prod' => ['required'],
            'description_prod' => ['required'],
            'prix' => ['required'],
            'image_id' => ['required']
        ])->validate();

        Product::create($request->all());
        return redirect()->back()->with('message', 'commoand created successfully.');
    }

    /**
     * Show the form for creaing a new resource.
     * 
     * @return Response
     */
    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'nom_prod' => ['required'],
            'description_prod' => ['required'],
            'prix' => ['required'],
            'image_id' => ['required']
        ])->validate();

        if($request->has('id'))
        {
            Product::find($request->input('id'))->update($request->all());
            return redirect()->back()->with('message', 'Product Updated successfully.');
        }
    }

    /**
     * Show the form for creaing a new resource.
     * 
     * @return Response
     */
    public function delete(Request $request)
    {
     
        $request->has('id') ? 
        Product::find($request->input('id'))->delete() :
                redirect()->back()
                    ->with('errors', 'Somethings goes wrong.');
        
        return redirect()->back()
                    ->with('message', 'Product deleted successfully.');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function index()
    {
        $data = Reservation::all();
        return Inertia::render('reservations', 
        [
            'data' => $data->latest()->get()
        ]);
    }

    /**
     * Show the form for creaing a new resource.
     * 
     * @return Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'date-debut' => ['required'],
            'date-fin' => ['required'],
            'nbre-place' => ['required'],
            'nom-revervant' => ['required'],
            'pays-revervant' => ['required'],
            'ville-revervant' => ['required'],
            'tel-revervant' => ['required']
        ])->validate();

        Reservation::create($request->all());
        return redirect()->back()->with('message', 'commoand created successfully.');
    }

    /**
     * Show the form for creaing a new resource.
     * 
     * @return Response
     */
    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'date-debut' => ['required'],
            'date-fin' => ['required'],
            'nbre-place' => ['required'],
            'nom-revervant' => ['required'],
            'pays-revervant' => ['required'],
            'ville-revervant' => ['required'],
            'tel-revervant' => ['required']
        ])->validat8000e();

        if($request->has('id'))
        {
            Reservation::find($request->input('id'))->update($request->all());
            return redirect()->back()->with('message', 'Reservation Updated successfully.');
        }
    }

    /**
     * Show the form for creaing a new resource.
     * 
     * @return Response
     */
    public function delete(Request $request)
    {
     
        $request->has('id') ? 
        Reservation::find($request->input('id'))->delete() :
                redirect()->back()
                    ->with('errors', 'Somethings goes wrong.');
        
        return redirect()->back()
                    ->with('message', 'Reservation deleted successfully.');
    }
}

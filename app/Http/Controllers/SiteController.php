<?php

namespace App\Http\Controllers;

use App\Models\SiteTouristique;
use Illuminate\Http\Request;
use Inertia\Inertia;

class SiteController extends Controller {
    public function index() {
        $sites = SiteTouristique::join( 'pictures', 'site_touristiques.image_id', 'pictures.id' )
        ->get();
        return Inertia::render( 'Sites/Index', [
            'sites' => $sites
        ] );
    }

    public function show($id ) {
        $site = SiteTouristique::find( 2 );
        // dd( $site );
        return Inertia::render( 'Sites/Show' );
    }

    public function add() {
        return Inertia::render( 'Sites/AddSiteForm' );
    }
}

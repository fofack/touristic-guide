<?php

namespace App\Models;

use App\Models\SiteTouristique;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Picture extends Model {
    use HasFactory;

    public function sites() {
        return $this->belongsTo( SiteTouristique::class );
    }
}

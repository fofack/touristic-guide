<?php

namespace App\Models;

use App\Models\Picture;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SiteTouristique extends Model {
    use HasFactory;

    public function pictures() {
        return $this->hasMany( Picture::class );
    }
}

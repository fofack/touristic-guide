<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteTouristiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_touristiques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_st');
            $table->text('description_st');
            $table->string('localisation_st');
            $table->unsignedInteger('image_id');
            $table->foreign('image_id')->references('id')->on('pictures')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_touristiques');
    }
}

<?php

namespace Database\Factories;

use App\Models\Logement;
use App\Models\Picture;
use Illuminate\Database\Eloquent\Factories\Factory;

class LogementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Logement::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'description_log' => $this->faker->text(),
            'image_id' => Picture::all()->random()->id
        ];
    }
}

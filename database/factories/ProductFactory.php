<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Picture;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory {
    /**
    * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
    *
    * @return array
    */

    public function definition() {
        return [
            'nom_prod' => $this->faker->sentence(),
            'description_prod' => $this->faker->text,
            'prix' => $this->faker->numberBetween( $min = 1, $max = 50000 ),
            'image_id'=> Picture::all()->random()->id

        ];
    }
}

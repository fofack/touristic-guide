<?php

namespace Database\Factories;

use App\Models\SiteTouristique;
use App\Models\Picture;
use Illuminate\Database\Eloquent\Factories\Factory;

class SiteTouristiqueFactory extends Factory {
    /**
    * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SiteTouristique::class;

    /**
     * Define the model's default state.
    *
    * @return array
    */

    public function definition() {
        return [
            'nom_st' => $this->faker->sentence(),
            'description_st' => $this->faker->text( 500 ),
            'localisation_st' => $this->faker->sentence(),
            'image_id' => Picture::all()->random()->id
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Vendeur;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class VendeurFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vendeur::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' =>$this->faker->sentence(),
            'prenom' =>$this->faker->sentence(),
            'pays' =>$this->faker->sentence(),
            'ville' =>$this->faker->sentence(),
            'tel' =>$this->faker->sentence(),
            'prod_id' => Product::all()->random()->id        ];
    }
}

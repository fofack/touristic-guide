<?php

namespace Database\Factories;

use App\Models\Command;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommandFactory extends Factory {
    /**
    * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Command::class;

    /**
     * Define the model's default state.
    *
    * @return array
    */

    public function definition() {
        return [
            'libelle_cmd' => $this->faker->text(),
            'date_livraison' => $this->faker->date(),
            'prod_id' => Product::all()->random()->id
        ];
    }
}

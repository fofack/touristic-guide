<?php

namespace Database\Factories;

use App\Models\Visiteur;
use App\Models\Reservation;
use App\Models\Command;
use Illuminate\Database\Eloquent\Factories\Factory;

class VisiteurFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Visiteur::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' =>$this->faker->sentence(),
            'prenom' =>$this->faker->sentence(),
            'pays' =>$this->faker->sentence(),
            'ville' =>$this->faker->sentence(),
            'tel' =>$this->faker->sentence(),
            'reservat_id' => Reservation::all()->random()->id,
            'command_id' => Command::all()->random()->id
        ];
    }
}

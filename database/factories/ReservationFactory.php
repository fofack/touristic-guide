<?php

namespace Database\Factories;

use App\Models\Reservation;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReservationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reservation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date-debut' => $this->faker->date(),
            'date-fin' => $this->faker->date(),
            'nbre-place' => $this->faker->numberbetween(1, 10),
            'nom-reservant' => $this->faker->sentence(),
            'pays-reservant' => $this->faker->sentence(),
            'ville-reservant' => $this->faker->sentence(),
            'tel-reservant' => $this->faker->sentence()

        ];
    }
}

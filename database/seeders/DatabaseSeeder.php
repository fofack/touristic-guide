<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Command;
use App\Models\Picture;
use App\Models\Product;
use App\Models\Vendeur;
use App\Models\Category;
use App\Models\Logement;
use App\Models\Visiteur;
use App\Models\Reservation;
use Illuminate\Database\Seeder;
use App\Models\SiteTouristique;

class DatabaseSeeder extends Seeder {
    /**
    * Seed the application's database.
    *
    * @return void
    */

    public function run() {
        User::factory( 10 )->create();
        Reservation::factory( 10 )->create();
        Picture::factory( 50 )->create();
        Logement::factory( 10 )->create();
        Product::factory( 10 )->create();
        Command::factory( 10 )->create();
        Category::factory( 10 )->create();
        Visiteur::factory( 10 )->create();
        Vendeur::factory( 10 )->create();
        SiteTouristique::factory( 10 )->create();

    }
}

<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// });

Route::get("/", "App\Http\Controllers\HomeController@index")->name('home');
Route::get("/sites","App\Http\Controllers\SiteController@index")->name('site.index');
Route::get("/sites/{id}","App\Http\Controllers\SiteController@show");
Route::get("/boutique","App\Http\Controllers\ProduitController@index")->name('boutique.index');
Route::get("/logements","App\Http\Controllers\LogementController@index")->name('logement.index');
// Route::get("/login","App\Http\Controllers\SecurityController@login")->name('login');


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('DashboardSummary');
})->name('dashboard');

Route::group(['auth:sanctum','verified'], function(){
    Route::get("/addSites","App\Http\Controllers\SiteController@add")->name('site.add');
    Route::get("/boutique/addProduit","App\Http\Controllers\ProduitController@add")->name('boutique.addProduit');
    Route::get("/logements/add","App\Http\Controllers\LogementController@add")->name('logement.add');
    Route::get("/reservations","App\Http\Controllers\LogementController@index")->name('reservation.index'); 
});